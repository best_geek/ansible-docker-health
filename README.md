# Docker Health Check via Ansible  
The purpose of this is quite simple

- See if a docker is in a stopped state
    - If it is, restart it

- See if a docker is in an unhealthy state
    - If it is, restart it

In both scenarios, there will be ```DOCKER::ERROR``` that you can grep for in the playbook output

This should also be suffcient enough to tell you how ansible fixed the problem. 

(This will automatically work on containers that do not support healthy monitoring too)

## Get going
Make sure you have both yaml files in the same dir

Go into docker_manager to see something like the following

```
- hosts: somehost
  become: true
  become_user: root
  vars:
    cnames:
      - samba
      - fastlinks
      - portainer
      - plex
      - glances

  tasks:
    - include_tasks: "docker_actions.yaml"
      loop: "{{ cnames }}"
      loop_control:
        loop_var: cname
```

Replace the list of ```cnames``` with the list of containers you wish to monitor

The file also demonstrates the ability to split containers between specific hosts. 


### Filtering output
Common useful terms to grep for 
```DOCKER::ERROR``` - If a container required a restart or was in a bad state

```DOCKER::INFO``` - Informational. Currently this occurs if the container cannot support healthy monitoring
